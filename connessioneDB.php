<?php
error_reporting(E_ALL); // Report all PHP errors
error_reporting(-1); // Same as error_reporting(E_ALL);
ini_set('error_reporting', E_ALL);

$host = 'localhost';
$nomeutente = 'user_WP@localhost';
$password = 'pippo_2003';
$dbname = 'nasa';
try {  // connessione tramite creazione di un oggetto PDO
  $db = new PDO('mysql:charset=utf8;host=' . $host . ';dbname=' . $dbname, $nomeutente, $password);
  $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
}
// blocco catch per la gestione delle eccezioni
catch (PDOException $e) {
  echo 'Attenzione problemi di connessione al db  : ' . $e->getMessage() . $e->getCode();
}



function queryVisualizza($db, $date, $title, $expl, $url, $hdurl, $cpr)
{
  $query = $db->prepare("SELECT dataImage,title,explanation,urlImage,hdurl,copyright  FROM apod WHERE dataImage=:dateI AND title=:title AND explanation=:expl
        AND urlImage=:urlI AND hdurl=:hdurl AND copyright=:cpr;");

  $query->bindParam(':dateI', $date, PDO::PARAM_STR);
  $query->bindParam(':title', $title, PDO::PARAM_STR);
  $query->bindParam(':expl', $expl, PDO::PARAM_STR);
  $query->bindParam(':urlI', $url, PDO::PARAM_STR);
  $query->bindParam(':hdurl', $hdurl, PDO::PARAM_STR);
  $query->bindParam(':cpr', $cpr, PDO::PARAM_STR);

  //echo __LINE__;
  try {
    $query->execute();
    $apod = $query->fetchall();
    if ($apod > 0) {
      echo "<pre>" . $apod . "</pre>";
    } else {
      echo 0;
    }
  } catch (PDOException $e) {
    echo ($e->getMessage());
  }
  return $apod;
}

function queryInsert($db, $date, $title, $expl, $url, $hdurl, $cpr)
{
  $query = $db->prepare("INSERT INTO apod (dataImage,title,explanation,urlImage,hdurl,copyright) 
      VALUES (:dateI,:title,:expl,:urlI,:hdurl,:cpr);");

  $query->bindParam(':dateI', $date, PDO::PARAM_STR);
  $query->bindParam(':title', $title, PDO::PARAM_STR);
  $query->bindParam(':expl', $expl, PDO::PARAM_STR);
  $query->bindParam(':urlI', $url, PDO::PARAM_STR);
  $query->bindParam(':hdurl', $hdurl, PDO::PARAM_STR);
  $query->bindParam(':cpr', $cpr, PDO::PARAM_STR);

  //echo __LINE__;
  try {
    $query->execute();
    /*
        $apod = $query->fetchall();
        if ($apod > 0) {
        } else {
          echo 0;
        }
        */
  } catch (PDOException $e) {
    echo ($e->getMessage());
  }
}

function queryControl($db, $date, $title, $expl, $url, $hdurl, $cpr)
{
  $query = $db->prepare("SELECT * FROM apod  WHERE dataImage=:dateI AND urlImage=:urlI");

  $query->bindParam(':dateI', $date, PDO::PARAM_STR);
  $query->bindParam(':urlI', $url, PDO::PARAM_STR);


  //echo __LINE__;
  try {

    $query->execute();

    $apod = $query->fetchcolumn(0);
    if ($apod > 0) {
      echo "if" . $apod;
    } else {
      queryInsert($db, $date, $title, $expl, $url, $hdurl, $cpr);
    }
  } catch (PDOException $e) {
    echo ($e->getMessage());
  }
}
